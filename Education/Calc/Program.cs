﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc
{
    class Calculator
    {
        public static double CalculateAddition(double a, double b)
        {
            double result = a + b;
            return result;
        }

        public static double CalculateSubstraction(double a, double b)
        {
            double result = a - b;
            return result;
        }

        public static double CalculateMultiplication(double a, double b)
        {
            double result = a * b;
            return result;
        }

        public static double CalculateDivision(double a, double b)
        {
            double result = a / b;
            return result;
        }

        static bool IsNumeric(object Expression)
        {
            double retNum;

            bool isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        static void Main()
        {
            Console.WriteLine("Enter the number");
            string inputA = Console.ReadLine();
            double valueA ;
            double valueB;

            valueA = Convert.ToDouble(inputA);

            string inputB = Console.ReadLine();
            valueB = Convert.ToDouble(inputB);

            Console.WriteLine("Calculated Addition {0}", CalculateAddition(valueA, valueB));
            Console.WriteLine("Calculated Substraction {0}", CalculateSubstraction(valueA, valueB));
            Console.WriteLine("Calculated Multiplication {0}", CalculateMultiplication(valueA, valueB));
            Console.WriteLine("Calculated Division {0}", CalculateDivision(valueA, valueB));

            Console.ReadKey();
            }            
    }
}
