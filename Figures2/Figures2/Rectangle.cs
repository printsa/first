﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures2
{
    class Rectangle : Figure
    {
        public Rectangle(Point startPosition, double distanceFromCenter)
        {
            this.Name = "Square";
            this.SidesAmount = 4;
            this.DistanceFromCenter = distanceFromCenter;
            BuildFigure(startPosition);
            CalculateSideLength();

        }
        public override double Resize(double resizeCoefficient)
        {
            return this.DistanceFromCenter *= resizeCoefficient;
        }

        public override double CalculateSideLength()
        {
            List<Point> figure = this.GetFigure;
            Point coordinate1 = figure[0];
            Point coordinate2 = figure[1];

            double sideLength = Math.Sqrt(Math.Pow(coordinate2.X - coordinate1.X, 2) + Math.Pow(coordinate2.Y - coordinate1.Y, 2));
            return sideLength;
        }

        public override double CalculateArea()
        {
            double calculatedArea = 2*Math.Pow(this.DistanceFromCenter, 2);
            return calculatedArea;
        }
    }
}