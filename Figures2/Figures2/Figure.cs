﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures2
{
    abstract class Figure
    {
        private List<Point> figureCoordinates = new List<Point>();
       
        public string Name { get; set; }
        public double DistanceFromCenter { get; set; }
        public int SidesAmount { get; set; }
        public List<Point> GetFigure { get { return figureCoordinates; } }
        public double SideLength { get { return this.CalculateSideLength(); } }
        public double Area { get { return this.CalculateArea(); } }
        
     
        public void BuildFigure(Point startPosition)
        {
            double x = startPosition.X;
            double y = startPosition.Y;

            Point position = new Point(x, y);

            int i = 0;
            double z = 0;
            double angle = 360.0 / this.SidesAmount;
            while (i < this.SidesAmount)
            {
                position.X = x + (Math.Round(Math.Cos(z / 180 * Math.PI) * this.DistanceFromCenter));
                position.Y = y - (Math.Round(Math.Sin(z / 180 * Math.PI) * this.DistanceFromCenter));
                figureCoordinates.Insert(i, position);
                z = z + angle;
                i++;
            }
        }

        public abstract double Resize(double resizeCoefficient);
        public abstract double CalculateSideLength();
        public abstract double CalculateArea();
    }
}
