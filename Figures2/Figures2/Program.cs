﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Figures2
{
    class Program
    {
        public static bool isFigureValid = false;
        public static bool isPositionEntered = false;
        public static bool isSizeEntered = false;
        private static double oldArea;

        static void Main()
        {
            ShowInstructionMessage();
            while (!isFigureValid && !isPositionEntered && !isSizeEntered)
            {
                ShowEnterTheFigureMessage();
                string inputFigure = Console.ReadLine();
                CloseConsole(inputFigure);
                if (ValidateEnteredFigure(inputFigure) && inputFigure != "random")
                {
                    ShowSpecifyStartPosition();
                    while (!isPositionEntered && !isSizeEntered)
                    {
                        ShowEnterX();

                        string inputPositionX = Console.ReadLine();
                        CloseConsole(inputPositionX);
                        if (ValidateEnteredPosition(inputPositionX))
                        {
                            isPositionEntered = false;
                            while (!isPositionEntered && !isSizeEntered)
                            {
                                ShowEnterY();

                                string inputPositionY = Console.ReadLine();
                                CloseConsole(inputPositionY);
                                if (ValidateEnteredPosition(inputPositionY))
                                {
                                    Point startPosition = new Point();
                                    startPosition.X = double.Parse(inputPositionX);
                                    startPosition.Y = double.Parse(inputPositionY);

                                    while (!isSizeEntered)
                                    {
                                        ShowEnterSizeMessage();
                                        string figureSize = Console.ReadLine();
                                        CloseConsole(figureSize);
                                        if (ValidateEnteredSize(figureSize))
                                        {
                                            double figureSized = double.Parse(figureSize);
                                            Figure createdFigure = CreateFigure(inputFigure, startPosition, figureSized);
                                            Console.WriteLine("{0,0} {1,20}",
                                                              "Figure: " + createdFigure.Name,
                                                              "Area: " + createdFigure.Area
                                            );

                                            Console.WriteLine("Enter resize coefficient");
                                            double resize = double.Parse(Console.ReadLine());
                                            createdFigure.Resize(resize);
                                            Console.WriteLine("{0,0} {1,20}",
                                                              "Figure: " + createdFigure.Name,
                                                              "New Area: " + createdFigure.Area
                                            );
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (inputFigure == "random")
                {
                    List<Figure> AllFigures = CreateRandomFigures();
                    Random c = new Random();

                    foreach (var figure in AllFigures.OrderBy(o => o.Area))
                    {
                        oldArea = figure.Area;
                        double coef = c.Next(1, 10);
                        figure.Resize(coef);

                        /*StringBuilder text = new StringBuilder();*/

                        Console.WriteLine("{0,0} {1,20} {2,-20} {3,-0}",
                            "Figure: " + figure.Name,
                            "Area: " + oldArea,
                            "Resize Coefficient: " + coef,
                            "New Area: " + figure.Area
                            );
                    }

                }
            }
            Console.ReadKey();
        }

        public static void ShowInstructionMessage()
        {
            Console.WriteLine("Build the figure. Enter 'square', 'triangle' or 'circle' to create appropriate figure.");
            Console.WindowWidth = 150;
            Console.WriteLine("Enter 'random' to create massive with 10 random figures.");
            Console.WriteLine("Enter 'quit' to close the app.");
        }
        public static void ShowEnterTheFigureMessage()
        {
            Console.WriteLine("Please enter the figure you want to build");
        }
        public static void ShowSpecifyStartPosition()
        {
            Console.WriteLine("Set start position in X,Y format");
        }
        public static void ShowEnterX()
        {
            Console.WriteLine("Enter X:");
        }
        public static void ShowEnterY()
        {
            Console.WriteLine("Enter Y:");
        }
        public static void ShowEnterSizeMessage()
        {
            Console.WriteLine("Please enter the size of the figure");
        }

        public static void ShowFigureTypeIsNotDefinedMessage()
        {
            Console.WriteLine("Figure type is not defined.");
        }

        public static void ShowNotValidDataEnteredMessage()
        {
            Console.WriteLine("Entered value is not valid. Please try again");
        }

        public static Figure CreateFigure(string inputFigure, Point startPosition, double figureSize)
        {
            if (inputFigure == "square")
            {
                Figure figure = new Rectangle(startPosition, figureSize);
                return figure;
            }
            else if (inputFigure == "triangle")
            {
                Figure figure = new Triangle(startPosition, figureSize);
                return figure;
            }
            else if (inputFigure == "circle")
            {
                Figure figure = new Circle(startPosition, figureSize);
                return figure;
            }
            else
            {
                return null;
            }
        }
        public static bool ValidateEnteredFigure(string inputFigure)
        {
            if (inputFigure == "square")
            {
                isFigureValid = true;
                return isFigureValid;
            }
            else if (inputFigure == "triangle")
            {
                isFigureValid = true;
                return isFigureValid;
            }
            else if (inputFigure == "circle")
            {
                isFigureValid = true;
                return isFigureValid;
            }
            else if (inputFigure == "random")
            {
                isFigureValid = true;
                return isFigureValid;
            }
            else
            {
                isFigureValid = false;
                ShowFigureTypeIsNotDefinedMessage();
                return isFigureValid;
            }
        }
        public static bool ValidateEnteredPosition(string inputPosition)
        {
            if (inputPosition == "")
            {
                Console.WriteLine("Position is not set");
                isPositionEntered = false;
                return isPositionEntered;
            }
            else if (!inputPosition.All(char.IsDigit))
            {
                Console.WriteLine("Position is not set as valid number");
                isPositionEntered = false;
                return isPositionEntered;
            }
            else
            {
                isPositionEntered = true;
                return isPositionEntered;
            }
        }
        public static bool ValidateEnteredSize(string figureSize)
        {
            if (figureSize == "")
            {
                Console.WriteLine("Size is not set");
                isSizeEntered = false;
                return isSizeEntered;
            }
            else if (!figureSize.All(char.IsDigit))
            {
                Console.WriteLine("Size is not set as valid number");
                isSizeEntered = false;
                return isSizeEntered;
            }
            else
            {
                isSizeEntered = true;
                return isSizeEntered;
            }
        }
        public static void CloseConsole(string input)
        {
            if (input == "quit")
            {
                System.Environment.Exit(0);
            }
        }
        public static List<Figure> CreateRandomFigures()
        {
            List<string> figuresTypes = new List<string>() { "circle", "triangle", "square" };
            List<Figure> AllFigures = new List<Figure>();
            Random rnd = new Random();
            Point startPosition = new Point(rnd.Next(0, 255), rnd.Next(0, 255));
            for (int i = 0; i < 10; i++)
            {
                int j = rnd.Next(0, figuresTypes.Count);
                Figure figure = CreateFigure(figuresTypes.ElementAt(j), startPosition, rnd.Next(0, 25));
                AllFigures.Add(figure);
            }
            return AllFigures;
        }
    }
}