﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures2
{
    class Circle : Figure
    {
        public Circle(Point startPosition, double distanceFromCenter)
        {
            this.Name = "Circle";
            this.SidesAmount = 1;
            this.DistanceFromCenter = distanceFromCenter;

            BuildFigure(startPosition);
        }

        public override double Resize(double resizeCoefficient)
        {
            return this.DistanceFromCenter *= resizeCoefficient;
        }

        public override double CalculateSideLength()
        {
            return Math.PI * this.DistanceFromCenter * 2;
        }

        public override double CalculateArea()
        {
            double calculatedArea = Math.Pow(this.DistanceFromCenter, 2) * Math.PI;
            return calculatedArea;
        }
    }
}
