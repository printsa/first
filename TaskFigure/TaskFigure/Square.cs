﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskFigure
{
    class Square : Figure
    {
        private string name = "Square";
        public override string GetName { get { return this.name; } }

        public Square(double xPosition, double yPosition, double sidelength)
        {
            double [] initialPosition = this.StartPosition(xPosition, yPosition);
            this.SideLength = sidelength;

            double[][] square = new double[4][];

            square[0] = initialPosition;
            square[1] = new List<double> { initialPosition[0], initialPosition[1] - sidelength }.ToArray();
            square[2] = new List<double> { initialPosition[0] + sidelength, initialPosition[1] - sidelength }.ToArray();
            square[3] = new List<double> { initialPosition[0] + sidelength, initialPosition[1] }.ToArray();
        }

        
        public double SideLength { get; set; }

        public override void Resize(double resizeCoefficient)
        {
            double sideLength = this.SideLength;
            this.SideLength = sideLength * resizeCoefficient;
        }

        public override double CalculateArea()
        {
            double sideLength = this.SideLength;
            double squareArea = sideLength * sideLength;
            return squareArea;
        }

    }
}
