﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskFigure
{

    class Program
    {
        static void Main()
        {
            Figure square1 = new Square(0, 0, 4);

            Console.Write(square1.GetName());
            Console.WriteLine(square1.CalculateArea());

            square1.Resize(2);
            Console.WriteLine(square1.CalculateArea());
            
            Console.ReadKey();
        }
    }
}