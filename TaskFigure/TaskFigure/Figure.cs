﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskFigure
{
    abstract class Figure
    {
        public abstract string GetName{ get; }

        private double x, y;
        public double[] StartPosition(double x, double y)
        {
            this.x = x;
            this.y = y;

            double[] position = new double[2];
            position[0] = x; position[1] = y;

            return position;
        }

        public abstract void Resize(double resizeCoefficient);
        public abstract double CalculateArea();
    }
}